<?php
/**
 * Created by PhpStorm.
 * User: nguyenvanminh
 * Date: 08/05/2018
 * Time: 15:38
 */

/*
 * Decorator design pattern
 */

abstract class Computer
{
    abstract protected function description();
}

class HardDisk extends Computer
{

    public function description()
    {
        return " harddisk ";
    }
}

class CD extends Computer
{

    public function description()
    {
        return " CD ";
    }
}

class Monitor extends Computer
{
    public function description()
    {
        return " Monitor ";
    }
}

$harddisk = new HardDisk();
$moniter  = new Monitor();
$cd       = new CD();

echo $harddisk->description().'-'.$cd->description().'-'.$moniter->description();
