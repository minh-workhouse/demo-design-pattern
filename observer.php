<?php
/**
 * Created by PhpStorm.
 * User: nguyenvanminh
 * Date: 08/05/2018
 * Time: 16:09
 */

/*
 * Observer design pattern
 */

interface Subject
{
    public function registerObserver( $name);
    public function removeObserver($name);
    public function notificationObserver();

}

interface Observer
{
    public function update($operation, $record);
}

class PatternSubject implements Subject
{
    protected $observers = array();

    public function registerObserver($name)
    {
        array_push($this->observers, $name);
    }

    public function removeObserver($name)
    {
        foreach ($this->observers as $key => $observer) {
            if ($observer == $name) {
                unset($this->observers[$observer]);
            }
        }
    }

    public function notificationObserver()
    {
        foreach ($this->observers as $key => $observer) {
            $observer->update('Update', $observer);
        }
    }

}

class PatternObserver implements Observer
{
    public function update($operation, $record)
    {
        return $operation.' => '.$record;
    }
}

$sub = new PatternSubject();
$sub->registerObserver('Subject 1');
$sub->registerObserver('Subject 2');


$obs = new PatternObserver();

var_dump($sub);


