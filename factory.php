<?php
/**
 * Created by PhpStorm.
 * User: nguyenvanminh
 * Date: 08/05/2018
 * Time: 15:34
 */

/*
 * Factory Design pattern
 */

interface Connection
{
    public function connection();
}

class OracleConnection implements Connection
{
    public function connection()
    {
        return "Oracle connection";
    }
}

class MySQLConnection implements Connection
{
    public function connection()
    {
        return "MySQL connection";
    }
}

class MySQLServerConnection implements Connection
{
    public function connection()
    {
        return "SQL Server connection";
    }
}

class SecurityOracleConnection implements Connection
{
    public function connection()
    {
        return "Security Oracle connection";
    }
}

class ConnectionFactory
{
    public function DoConnection($type)
    {
        switch ($type) {
            case "Oracle":
                $con =  new OracleConnection();
                return $con->connection();
                break;
            case "Security Oracle":
                $con = new SecurityOracleConnection();
                return $con->connection();
                break;
            case "MySQL":
                $con =  new MySQLConnection();
                return $con->connection();
                break;
            case "SQL Server":
                $con = new MySQLServerConnection();
                return $con->connection();
                break;
            default:
                return "Connection failure";
                break;
        }
    }
}

$con = new ConnectionFactory();
echo $con->doConnection()."<br>";
echo $con->doConnection('Oracle')."<br>";
echo $con->doConnection('Security Oracle')."<br>";
echo $con->doConnection('MySQL')."<br>";
echo $con->doConnection('SQL Server')."<br>";